<?php
/**
 * Copyright (c) 2014 Alexey Volkov <alexey@volkoff.ru> 
 * This file is licensed under the Affero General Public License version 3 or
 * later.
 * See the COPYING-README file.
 */




  $user_id = $_POST['login'];
  $email = $_POST['email'];
  OCP\Util::writeLog('local', 'addUser', OCP\Util::INFO);
  OC_Local::registerUser($user_id, $email);
  OC_Util::obEnd();
