<?php
/**
 * Copyright (c) 2014 Alexey Volkov <alexey@volkoff.ru> 
 * Copyright (c) 2013 Thomas Müller <thomas.mueller@tmit.eu>
 * Copyright (c) 2013 Bart Visscher <bartv@thisnet.nl>
 * This file is licensed under the Affero General Public License version 3 or
 * later.
 * See the COPYING-README file.
 */



use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LocalScanShare extends Command {


	protected function configure() {
		$this
			->setName('scanpath')
			->setDescription('Rescan path')
			->addArgument(
					'path',
					InputArgument::REQUIRED,
					'will rescan all files the given path'
				     )
		;
	}

	protected function scanFiles($share_path, OutputInterface $output) {
		$scanner = new \OC\Files\Utils\Scanner($share_path);
		$scanner->listen('\OC\Files\Utils\Scanner', 'scanFile', function($path) use ($output) {
			$output->writeln("Scanning <info>$path</info>");
		});
		$scanner->listen('\OC\Files\Utils\Scanner', 'scanFolder', function($path) use ($output) {
			$output->writeln("Scanning <info>$path</info>");
		});
		$scanner->scan('');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$share_path = $input->getArgument('path');
		$this->scanFiles($share_path, $output);
	}
}
