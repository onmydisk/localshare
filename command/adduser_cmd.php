<?php
/**
 * Copyright (c) 2014 Alexey Volkov <alexey@volkoff.ru> 
 * Copyright (c) 2013 Thomas Müller <thomas.mueller@tmit.eu>
 * Copyright (c) 2013 Bart Visscher <bartv@thisnet.nl>
 * This file is licensed under the Affero General Public License version 3 or
 * later.
 * See the COPYING-README file.
 */



use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

require_once 'lib/base.php';
require_once 'apps/local/lib/local.php';


class LocalAddUser extends Command {

	

	protected function configure() {
		$this
			->setName('adduser')
			->setDescription('Creates new user account')
			->addArgument(
					'login',
					InputArgument::REQUIRED,
					'User name'
				     )
			->addArgument(
					'email',
					InputArgument::REQUIRED,
					'User email'
				     )
		;
	}

	protected function addUser($user_id, $email, OutputInterface $output) {
		OC_Local::addUser($user_id, $email);


		$output->writeln("Account created for $user_id.");

	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$user_id = $input->getArgument('login');
		$email = $input->getArgument('email');
		$this->addUser($user_id, $email, $output);
	}
}
