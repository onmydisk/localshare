<?php
/**
 * Copyright (c) 2014 Alexey Volkov <alexey@volkoff.ru>
 * This file is licensed under the Affero General Public License version 3 or
 * later.
*/

$this->create('core_password_reset_password', '/password/reset/{token}/{user}')
	->post()
	->action('OC\Core\LostPassword\Controller', 'resetPassword');


