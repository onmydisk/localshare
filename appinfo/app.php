<?php

/**
 * ownCloud - local mount App
 *
 * @author Alexey Volkov, based on external app by Frank Karlitschek
 * @copyright 2014 Alexey Volkov alexey@volkoff.ru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

OC::$CLASSPATH['OC_Local'] = 'local/lib/local.php';
OCP\Util::addStyle( 'local', 'style');

OCP\App::registerPersonal('local', 'personal');

