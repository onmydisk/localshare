<?php
/**
 * Copyright (c) 2013 Bart Visscher <bartv@thisnet.nl>
 * This file is licensed under the Affero General Public License version 3 or
 * later.
 * See the COPYING-README file.
 */

OC::$CLASSPATH['LocalScanShare'] = 'local/command/scan.php';
OC::$CLASSPATH['LocalAddUser'] = 'local/command/adduser_cmd.php';
$application->add(new LocalScanShare());
//$application->add(new LocalAddUser());
