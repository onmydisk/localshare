<?php $TRANSLATIONS = array(
"My Network" => "Моя Сеть",
"Host name" => "Имя устройства",
"Certificate" => "Сертификат",
"File server" => "Файловый сервер",
"Remove" => "Удалить",
"Mount" => "Подключить",
"Add" => "Добавить",
"cloud storage on YOUR disk." => "облачное хранилище на ТВОЁМ диске.",
"Valid from" => "Выдан",
"Valid to" => "Действителен до",
"Serial number" => "Серийный номер",
"It's safe. It's free. It rocks when" => "Безопасно, удобно, без ограничений.",
"registered." => "Регистрируйтесь :)"
);
