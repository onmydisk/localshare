<?php $TRANSLATIONS = array(
"My Network" => "Moje omreže",
"Host name" => "Ime naprave",
"Certificate" => "Certifikat",
"File server" => "Podatkovni strežnik",
"Remove" => "Odstrani",
"Add" => "Dodaj",
"cloud storage on YOUR disk." => "oblačna shramba na TVOJEM disku",
"Valid from" => "Izdan",
"Valid to" => "Velja do",
"Serial number" => "Serijska številka",
"It's safe. It's free. It rocks when" => "Varno, zanesljuvo, neomejeno.",
"registered." => "Prijavi se!"
);
