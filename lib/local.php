<?php

/**
 * ownCloud - local mount App
 *
 * @author Alexey Volkov, inspired by external app by Frank Karlitschek
 * @copyright 2014 Alexey Volkov alexey@volkoff.ru
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

class OC_Local {

	


	public static function getKeys() {
		$keys = array();

		$args = array(OCP\USER::getUser());
		$sql = 'SELECT `id`, `key_name`, `key`, `mounted` FROM `*PREFIX*local` WHERE `user_id` = ?';

		$query = OCP\DB::prepare($sql);
		$result = $query->execute($args);
		
		while($row = $result->fetchRow()) {
        		array_push($keys, array($row["id"], $row["key_name"],$row["key"], $row["mounted"]));
		}
		
		return $keys;
	}




	/**
	*
	* @param id share id
	* @param string Mount point
	* @return bool
	*/
	public static function setShareName($id, $mountPoint) {
 		$class = '\OC\Files\Storage\Local';
		$mountType = 'user';
		$user = OCP\User::getUser();
		$isPersonal = false;

		//get share old name and path
		$args = array($id);
		$sql = 'SELECT `key_name`, `share_path`, `mounted`  FROM `*PREFIX*local` WHERE `id` = ?';
		$query = OCP\DB::prepare($sql);
		$result = $query->execute($args);
		$row = $result->fetchRow();

		$share_path = $row["share_path"];
		$share_name = $row["key_name"];
		$mounted = $row["mounted"];

		$oldMountPoint = '/'.$user.'/files/'.ltrim($share_name, '/');		

		//update row
		$args = array($mountPoint, $id);
		$sql = "UPDATE `*PREFIX*local` SET `key_name` = ? WHERE `id` = ?";
		$query = OCP\DB::prepare($sql);
		$result = $query->execute($args);


		//share options
		$classOptions = array('datadir' => '/users/'.$user.'/'.$share_path);
		$mountPoint = OC\Files\Filesystem::normalizePath($mountPoint);
		$isPersonal = false;
		if ($mountPoint === '' || $mountPoint === '/' || $mountPoint == '/Shared') {
			// can't mount at root or "Shared" folder
			return false;
		}
		$mountPoint = '/'.$user.'/files/'.ltrim($mountPoint, '/');


		$mount = array($user => array($mountPoint => array('class' => $class, 'options' => $classOptions)));
		$mountPoints = self::readData($isPersonal);

		// Remove old mount point
		unset($mountPoints[$mountType][$user][$oldMountPoint]);
		// Unset parent arrays if empty
		if (empty($mountPoints[$mountType][$user])) {
			unset($mountPoints[$mountType][$user]);
			if (empty($mountPoints[$mountType])) {
				unset($mountPoints[$mountType]);
			}
		}



		// Merge the new mount point into the current mount points
		if (isset($mountPoints[$mountType])) {
			if (isset($mountPoints[$mountType][$user])) {
				$mountPoints[$mountType][$user]
					= array_merge($mountPoints[$mountType][$user], $mount[$user]);
			} else {
				$mountPoints[$mountType] = array_merge($mountPoints[$mountType], $mount);
			}
		} else {
			$mountPoints[$mountType] = $mount;
		}
		self::writeData($isPersonal, $mountPoints);

		// OCP\Util::writeLog('local', 'Mount point '. $mountPoint . ' set to '. $share_path, OCP\Util::INFO);

		return $mounted;
	}

	/**
	*
	* @param id share id
	* @param string Mount point
	* @return bool
	*/
	public static function removeShare($id, $mountPoint) {
		// Verify that the mount point applies for the current user
		$mountType = 'user';
		$user = OCP\User::getUser() ;
		$isPersonal = false;
		$mountPoint = '/'.$user.'/files/'.ltrim($mountPoint, '/');
		
		//remove row from DB
		$args = array($id);
		$sql = "DELETE FROM `*PREFIX*local` WHERE `id` = ?";
		$query = OCP\DB::prepare($sql);
		$result = $query->execute($args);

		$mountPoints = self::readData($isPersonal);
		// Remove mount point
		unset($mountPoints[$mountType][$user][$mountPoint]);
		// Unset parent arrays if empty
		if (empty($mountPoints[$mountType][$user])) {
			unset($mountPoints[$mountType][$user]);
			if (empty($mountPoints[$mountType])) {
				unset($mountPoints[$mountType]);
			}
		}
		
		self::writeData($isPersonal, $mountPoints);
		return true;
	}

	/**
	* Read the mount points in the config file into an array
	* @param bool Personal or system config file
	* @return array
	*/
	private static function readData($isPersonal) {
		$parser = new OC\ArrayParser();
		if ($isPersonal) {
			$phpFile = OC_User::getHome(OCP\User::getUser()).'/mount.php';
			$jsonFile = OC_User::getHome(OCP\User::getUser()).'/mount.json';
		} else {
			$datadir = OC_Config::getValue("datadirectory", \OC::$SERVERROOT . "/data");
			$phpFile = OC::$SERVERROOT.'/config/mount.php';
			$jsonFile = $datadir . '/mount.json';
		}
		if (is_file($jsonFile)) {
			$mountPoints = json_decode(file_get_contents($jsonFile), true);
			if (is_array($mountPoints)) {
				return $mountPoints;
			}
		} elseif (is_file($phpFile)) {
			$mountPoints = $parser->parsePHP(file_get_contents($phpFile));
			if (is_array($mountPoints)) {
				return $mountPoints;
			}
		}
		return array();
	}

	/**
	* Write the mount points to the config file
	* @param bool Personal or system config file
	* @param array Mount points
	*/
	private static function writeData($isPersonal, $data) {
		if ($isPersonal) {
			$file = OC_User::getHome(OCP\User::getUser()).'/mount.json';
		} else {
			$datadir = OC_Config::getValue("datadirectory", \OC::$SERVERROOT . "/data");
			$file = $datadir . '/mount.json';
		}
		$content = json_encode($data);
		@file_put_contents($file, $content);
		@chmod($file, 0640);
	}



//creates new user account

	public static function addUser($user_id, $email) {

		//check if not empty

		if(empty($user_id) || empty($email)) {
			return "invalid params";
		}


		//check if exists

		$args = array($user_id);
		$sql = 'SELECT * FROM `*PREFIX*users` WHERE `uid` = ?';
		$query = OCP\DB::prepare($sql);
		$result = $query->execute($args);
		
		if($row = $result->fetchRow()) {
			return "invalid user";
		}

		$args = array($email);
		$sql = 'SELECT * FROM `*PREFIX*preferences` WHERE `configvalue` = ?';
		$query = OCP\DB::prepare($sql);
		$result = $query->execute($args);
		
		if($row = $result->fetchRow()) {
			return "invalid email";
		}

		//add user
		$args = array($user_id);
		$sql = "INSERT INTO `*PREFIX*users` (`uid`)   VALUES (?)";
		$query = OCP\DB::prepare($sql);
		$result = $query->execute($args);

		 //setup preferences
		 $args = array($user_id, "files", "cache_version", "5");

		 $sql = "INSERT INTO *PREFIX*preferences 
			(`userid`, `appid` , `configkey`, `configvalue`) 
                        VALUES (?, ?, ?, ?) ";
		 $query = OCP\DB::prepare($sql);
		 $result = $query->execute($args);

		 $args = array($user_id, "settings", "email", $email);

		 $sql = "INSERT INTO *PREFIX*preferences 
			(`userid`, `appid` , `configkey`, `configvalue`) 
                        VALUES (?, ?, ?, ?) ";
		 $query = OCP\DB::prepare($sql);
		 $result = $query->execute($args);

		 $token = hash('sha256', OC_Util::generateRandomBytes(30).OC_Config::getValue('passwordsalt', ''));
		 OC_Preferences::setValue($user_id, 'owncloud', 'lostpassword', hash('sha256', $token)); 
		 $link = "https://cloud.onmydisk.com/index.php/lostpassword/reset/$token/$user_id";
		 return $link;

	}

	public static function getPubInfo() {
		$pubinfo = $l->t("It's safe. It's free. It rocks when") . "<a href=\"".$this->defaultBaseUrl."/start.html#register\" target=\"_blank\">".  $this->l->t("registered.") . "</a>";
		return $pubinfo;

	}
		 

}

