<?php


/**
  * @author Alexey Volkov, inspired by external app by Frank Karlitschek
 * @copyright 2014 Alexey Volkov alexey@volkoff.ru
 *
 * This file is licensed under the Affero General Public License version 3 or later.
 * See the COPYING-README file.
 */

OCP\Util::addScript('local', 'settings' );
OCP\Util::addStyle('local', 'style');

$tmpl = new OCP\Template( 'local', 'settings');


return $tmpl->fetchPage();
