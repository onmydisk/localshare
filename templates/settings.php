
<form id="local">
	<fieldset class="personalblock">
		<h2><?php p($l->t('My Network'));?></h2>
		<?php if (isset($_['dependencies']) and ($_['dependencies']<>'')) print_unescaped(''.$_['dependencies'].''); ?>
		<table class="grid" id="keys_grid" >
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><?php p($l->t('Host name')); ?></th>
					<th><?php p($l->t('Certificate')); ?></th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody width="100%">
		<?php
		$keys = OC_Local::getKeys();
		for($i = 0; $i < sizeof($keys); $i++) {

		?>
		<tr class="key_row" id="key_row">
				<td class="status"><input type="hidden" id="id" name="id[]" value="<?php p(OCP\Util::sanitizeHTML($keys[$i][0])); ?>"/><?php $keys[$i][3] ? print_unescaped('<span id="status" class="success"></span>') :  print_unescaped('<span id="status" class="error"></span>') ; ?></td>
				<td class="key_name"><input type="text" name="key_name[]"  value="<?php p(OCP\Util::sanitizeHTML($keys[$i][1])) ; ?>" placeholder="<?php p($l->t('Name')) ; ?>" /></td>
				<?php
					$data = openssl_x509_parse($keys[$i][2]);
					$validFrom = date('Y-m-d H:i:s', $data['validFrom_time_t']);
					$validTo = date('Y-m-d H:i:s', $data['validTo_time_t']);
					$serialNum = $data['serialNumber'];
					$key_value = $l->t('Valid from').": ".$validFrom.", ".$l->t('Valid to').": ".$validTo.", ".$l->t('Serial number').": ".$serialNum;
				?>
				<td class="key"><input type="text" readonly name="key[]" value="<?php p(OCP\Util::sanitizeHTML($key_value)) ; ?>" placeholder="<?php p($l->t('Key')) ; ?>" /></td>
				
		<td class="remove"><img  class="svg action delete" src="<?php print_unescaped(image_path('core', 'actions/delete.svg')); ?>" title="<?php p($l->t('Remove')); ?>"/></td>

		</tr>
 	<?php } ?>

	</tbody>
	
        </table>


	</fieldset>
</form>
