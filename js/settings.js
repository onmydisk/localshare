$(document).ready(function(){
    	newRowHtml = '<tr><td class="status"><span id="status"></span><input type="hidden" id="id" name="id[]" value="" /></td><td class="key_name"><input type="text" id="key_name" name="key_name[]" value="" placeholder="Name" /></td><td class="key"><input type="text" name="key[]" id="key" value=""  placeholder="Key" /></td><td class="mount"><input type="checkbox" name="mounted[]" id="mounted" value="1"  title="Mount" /></td><td class="remove"><img class="svg action delete" src="'+OC.imagePath("core", "actions/delete") +'" title="Remove" /></td></tr>';

	// Handler functions
	function addKeyEventHandler(event) {
		event.preventDefault();

		saveKey($(this).parent().parent());
	}

	function mountCheckEventHandler(event) {
		event.preventDefault();

		saveKey($(this).parent().parent());
	}

	function deleteButtonEventHandler(event) {
		event.preventDefault();

		$(this).tipsy('hide');
		$(this).parent().parent().remove();

		deleteKey($(this).parent().parent());
	}

	
	function saveKey(tr) {
		  
		  var id = $(tr).find('#id').val();
		  var key_name = $(tr).find('.key_name input').val();
		  var key = $(tr).find('.key input').val();
		  var mounted = $(tr).find('.mount input').prop('checked');
		  var statusSpan = $(tr).closest('tr').find('.status span');

		  statusSpan.addClass('loading-small').removeClass('error success none');
		  $.ajax({type: 'POST',
		 	   url: OC.filePath('local', 'ajax', 'setkey.php'),
			  data: {
			        	id: id,
				  key_name: key_name,
				       key: key,
				   mounted: mounted,
				},
			success: function(data) {
					statusSpan.addClass(data.result).removeClass('loading-small');
					$(tr).find('#id').val(data.id);
			},
			error: function(data){
					statusSpan.addClass('error').removeClass('loading-small');
			}
		});	
		 
	}

	function deleteKey(tr) {
		  
		  var id = $(tr).find('#id').val();
		  var key_name = $(tr).find('.key_name input').val();
		  var statusSpan = $(tr).closest('tr').find('.status span');
		  statusSpan.addClass('loading-small').removeClass('error success none');
		  $.ajax({type: 'POST',
		 	   url: OC.filePath('local', 'ajax', 'deletekey.php'),
			  data: {
			        	id: id,
				  key_name: key_name,
				},
			success: function(data) {
					$(tr).remove();
			},
			error: function(data){
					$(tr).remove();
			}
		});	
		 
	}

	// Initialize events
	$('input[name^=key]').change(addKeyEventHandler);
	$('input[name^=mounted]').change(mountCheckEventHandler);
	$('img.delete').click(deleteButtonEventHandler);

	$('#add_key').click(function(event) {
		event.preventDefault();
		$('#keys_grid tbody:last').append(newRowHtml);

		$('input[name^=key]').change(addKeyEventHandler);
		$('input[name^=mounted]').change(mountCheckEventHandler);
		$('img.delete').click(deleteButtonEventHandler);
		$('img.delete:last').tipsy();

	});

});
