<?php

/**
 * @author Alexey Volkov, inspired by external app by Frank Karlitschek
 * @copyright 2014 Alexey Volkov alexey@volkoff.ru
 *
 * This file is licensed under the Affero General Public License version 3 or later.
 * See the COPYING-README file.
 */


OCP\User::checkLoggedIn();


$id = 	strip_tags($_POST['id']);
$key_name = strip_tags($_POST['key_name']);

$user = OCP\USER::getUser(); 
$old_mounted = false;
$result="none";

//remove old mount point
if (! empty($id) && !empty($key_name))
{

		if (OC_Local::setShareName($id, $key_name))
			$result="success";	
		else
			$result="error";	

}	


OCP\JSON::success(array('result'=>$result, 'id'=>$id));

