<?php

/**
  * @author Alexey Volkov, inspired by external app by Frank Karlitschek
 * @copyright 2014 Alexey Volkov alexey@volkoff.ru
 *
 * This file is licensed under the Affero General Public License version 3 or later.
 * See the COPYING-README file.
 */


OCP\User::checkLoggedIn();


$id = 	strip_tags($_POST['id']);
$key_name = strip_tags($_POST['key_name']);



if (!empty($key_name) && !empty($id) ) {

		OC_Local::removeShare($id, $key_name) ;

}

$result = "success";
OCP\JSON::success(array('result'=>$result));

